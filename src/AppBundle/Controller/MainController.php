<?php


namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @return mixed
     */
    public function homepageAction(){
        return $this->render('main/homepage.html.twig');
    }


}