<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use AppBundle\Service\MarkdownTransformer;
use Faker\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GenusController extends Controller
{
    /**
     * @Route("/genus/new")
     */
    public function newAction(){
        $faker = Factory::create();
        $em = $this->getDoctrine()->getManager();
        $subFamilies = $em->getRepository('AppBundle\Entity\SubFamily')
            ->findAll();
        for($i=0;$i<100;$i++){
            $genus = (new Genus())
                ->setName($faker->name())
                ->setSubFamily($subFamilies[array_rand($subFamilies)])
                ->setSpeciesCount(rand(10,10000))
                ->setFunFact($faker->text(255))
                ->setIsPublished($faker->boolean(85))
                ->setFirstDiscoveredAt($faker->dateTimeBetween("-500 years", "-5 years"))
            ;
            $em->persist($genus);
        }
        $em->flush();


        return new Response('100 genuses created');
    }

    /**
     * @Route("/genus", name="genuses")
     */
    public function listAction(){
        $em = $this->getDoctrine()->getManager();
        $genuses = $em->getRepository('AppBundle\Entity\Genus')
            ->findAllPublishedOrderedByRecentlyActive();




        return $this->render('genus/list.html.twig', [
            'genuses' => $genuses
        ]);
    }

    /**
     * @Route("/genus/{genusName}", name="genus_show")
     */
    public function showAction($genusName)
    {
        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository('AppBundle:Genus')
            ->findOneBy(['name' => $genusName]);

        if(!$genus){
            throw new NotFoundHttpException("Genus not found");
        }

        $transformer = $this->get('app.markdown_transformer');
        $funFact = $transformer->parse($genus->getFunFact());

        /*
            sleep(1);
            $funfact = $this->get('markdown.parser')
                ->transform($funfact);

        }*/

        $recentNotes = $em->getRepository('AppBundle:GenusNote')
            ->findAllRecentNotesForGenus($genus);

        return $this->render('genus/show.html.twig', array(
            'genus' => $genus,
            'funFact' => $funFact,
            'recentNoteCount' => count($recentNotes),
        ));
    }

    /**
     * @Route("/genus/{name}/notes", name="genus_show_notes")
     * @Method("GET")
     */
    public function getNotesAction(Genus $genus)
    {
        $notes = [];
        foreach ($genus->getNotes() as $note){
            $notes[] = [
                'id' => $note->getId(),
                'username' => $note->getUsername(),
                'avatarUri' => '/images/'.$note->getUserAvatarFilename(),
                'note' => $note->getNote(),
                'date' => $note->getCreatedAt()->format('Y.m.d.')
            ];
        }

        $data = [
            'notes' => $notes
        ];

        return new JsonResponse($data);
    }
}
