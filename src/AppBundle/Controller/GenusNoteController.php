<?php
/**
 * Created by PhpStorm.
 * User: KrisztiánEgri
 * Date: 2019. 03. 29.
 * Time: 17:53
 */

namespace AppBundle\Controller;


use AppBundle\Entity\GenusNote;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenusNoteController extends Controller
{

    /**
     * @Route("/genusNote/new")
     */
    public function newAction(){
        $faker = Factory::create();
        $em = $this->getDoctrine()->getManager();

        $genuses = $em->getRepository('AppBundle\Entity\Genus')
            ->findAllPublished();


        for($i=0;$i<100;$i++){
            $genusNote = (new GenusNote())
                ->setUsername($faker->userName)
                ->setUserAvatarFilename($faker->boolean(50) == true ? 'leanna.jpeg' : 'ryan.jpeg')
                ->setNote($faker->paragraph(5))
                ->setCreatedAt($faker->dateTimeBetween('-50 years', 'now'))
                ->setGenus($genuses[rand(0,count($genuses)-1)])
            ;
            $em->persist($genusNote);
        }
        $em->flush();


        return new Response('100 genus-notes created');
    }
}