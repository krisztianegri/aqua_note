<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SubFamily;
use AppBundle\Service\MarkdownTransformer;
use Faker\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SubFamilyController extends Controller
{
    /**
     * @Route("/subfamily/new")
     */
    public function newAction(){
        $faker = Factory::create();
        $em = $this->getDoctrine()->getManager();
        for($i=0;$i<25;$i++){
            $subFamily = new SubFamily();
            $subFamily->setName($faker->jobTitle);
            $em->persist($subFamily);
        }
        $em->flush();


        return new Response('25 subfamilies created');
    }


}
