<?php
/**
 * Created by PhpStorm.
 * User: KrisztiánEgri
 * Date: 2019. 03. 29.
 * Time: 17:30
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class GenusNoteRepository extends EntityRepository
{
    public function findAllRecentNotesForGenus(Genus $genus){
        return $this->createQueryBuilder('genus_note')
            ->andWhere('genus_note.genus = :genus')
            ->setParameter('genus', $genus)
            ->andWhere('genus_note.createdAt > :recentDate')
            ->setParameter('recentDate', new \DateTime('-3 years'))
            ->getQuery()
            ->execute();
    }
}