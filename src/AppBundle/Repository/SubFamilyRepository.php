<?php
/**
 * Created by PhpStorm.
 * User: KrisztiánEgri
 * Date: 2019. 03. 29.
 * Time: 17:30
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use AppBundle\Entity\SubFamily;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

class SubFamilyRepository extends EntityRepository
{
    public function createAlphabeticalQueryBuilder(){
        return $this->createQueryBuilder('sub_family')
            ->orderBy('sub_family.name', 'ASC');
    }
}